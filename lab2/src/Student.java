/**
 * Student la 1 ung dung khai bao cac thuoc tinh cua 1 sinh vien
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-09-22
 */
public class Student {
    private String name, id, group, email;

    /**
     * ben duoi la phuong thuc khoi tao khong co tham so va sinh vien
     * duoc tao ra se co gia tri voi cac thuoc tinh nhu:
     * name =“Student”, id=“000”, group=“K59CB”, email=”uet@vnu.edu.vn”
     *
     * @return ko tra ve gia tri nao
     */
    public Student() {
        this.name = "Student";
        this.id = "000";
        this.group = "K59CB";
        this.email = "uet@vnu.edu.vn";
    }

    /**
     * khoi tao cac thuoc tinh cho sinh vien
     * @param name  la ten sinh vien
     * @param id la ma sinh vien
     * @param group la lop cua sinh vien
     * @param email la email cua sinh vien
     * @return ko tra ve gia tri nao
     */
    public Student(String name, String id, String group, String email) {
        this.name = name;
        this.id = id;
        this.group = group;
        this.email = email;
    }


    /**
     * ben duoi la phuong thuc khoi tao ma sinh vien
     * duoc tao ra se co gia tri voi cac thuoc tinh
     * nhu: “name”, “id”, và“email”
     *
     * @param n   bien co dang string
     * @param sid bien co dang string
     * @param em  bien co dang string
     * @return ko tra ve gia tri nao
     */
    public Student(String n, String sid, String em) {
        this.name = n;
        this.id = sid;
        this.group = "K59CB";
        this.email = em;
    }

    /**
     * ben duoi la phuong thuc khoi sao chep, khi doi tuong
     * dc tao ra se giong voi thuoc tinh cua s
     *
     * @param s doi tuong dung de sao chep cac
     *          cac thuoc tinh
     * @return ko tra ve gia tri nao
     */
    public Student(Student s) {
        this.name = s.getName();
        this.id = s.getId();
        this.group = s.getGroup();
        this.email = s.getEmail();
    }

    /**
     * ben duoi la phuong thuc de cai dat ten sinh vien
     *
     * @param name ten sv
     * @return void ko tra ve gia tri nao
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * ben duoi la phuong thuc de lay ra ten cua sinh vien
     *
     * @return ko tra ve gia tri nao
     */
    public String getName() {
        return name;
    }
    /**
     * ben duoi la phuong thuc de cai dat ma sinh vien
     *
     * @param id ma so sv
     * @return void ko tra ve gia tri nao
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * ben duoi la phuong thuc de lay ra msv
     *
     * @return ko tra ve gia tri nao
     */
    public String getId() {
        return id;
    }

    /**
     * ben duoi la phuong thuc de cai dat ten lop cho
     * sinh vien
     *
     * @param group nhom hoc
     * @return void ko tra ve gia tri nao
     */
    public void setGroup(String group) {
        this.group = group;
    }
    /**
     * ben duoi la phuong thuc de lay ra ten lop cua
     * sinh vien
     *
     * @return ko tra ve gia tri nao
     */
    public String getGroup() {
        return group;
    }

    /**
     * ben duoi la phuong thuc de cai dat email cho
     * sinh vien
     *
     * @param email email cua sv
     * @return void ko tra ve gia tri nao
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * ben duoi la phuong thuc de lay ra email cua sinh
     * vien
     *
     * @return ko tra ve gia tri nao
     */
    public String getEmail() {
        return email;
    }

    /**
     * ben duoi la phuong thuc de in ra man hinh cac thuoc
     * tinh cua sinh vien
     *
     * @return void ko tra ve gia tri nao
     */
    public void printInfo() {
        System.out.println("Tên sv: " + this.name);
        System.out.println("Mã sv: " + this.id);
        System.out.println("Lớp: " + this.group);
        System.out.println("Email: " + this.email);

    }
}