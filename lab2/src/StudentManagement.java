/**
 * StudentManagement la 1 ung dung khoi tao 1 sv vao kiem tra
 * tinh dung dan cua cac phuong thuc trong de bai
 * lab2 yeu cau
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-09-22
 */
import java.util.ArrayList;
import java.util.Vector;

public class StudentManagement {

    private static ArrayList<Student> studentlist = new ArrayList<Student>();

    /**
     * main la phuong thuc tao va in ra thuoc tinh cua author
     * va thuc hien cac ham trong class
     * @param args khong su dung
     * @return ko tra ve
     */
    public static void main(String[] args) {
        // khoi tao cac hoc sinh
        Student stu1 = new Student("Hà Minh Hải", "17021238", "K59CLC1", "haihaidb@gmail.com");
        Student stu2 = new Student("Đỗ Thành Đạt", "17021230", "K59CLC2", "abcxzy@gmail.com");
        Student stu3 = new Student("Phạm Đức Duy", "17021228", "K60CAC", "xzyasd@gmail.com");
        Student stu4 = new Student("Nguyễn Hữu Đạt", "17021232", "K62DB", "hahaha@gmail.com");

        // in ra thong tin tung hoc sinh
        System.out.println("Student's name is: " + stu1.getName());
        System.out.println("Information of " + stu1.getName() + ":");
        stu1.printInfo();
        System.out.println("____________________________");

        System.out.println("Student's name is: " + stu2.getName());
        System.out.println("Information of " + stu2.getName() + ":");
        stu2.printInfo();
        System.out.println("____________________________");

        System.out.println("Student's name is: " + stu3.getName());
        System.out.println("Information of " + stu3.getName() + ":");
        stu3.printInfo();
        System.out.println("____________________________");

        System.out.println("Student's name is: " + stu4.getName());
        System.out.println("Information of " + stu4.getName() + ":");
        stu3.printInfo();
        System.out.println("____________________________");

        studentlist.add(new Student("abcd", "1111", "abcd", "abcd@.com"));
        studentlist.add(new Student("xasd", "1111", "xasd", "xasd@.com"));
        studentlist.add(new Student("hjgh", "2222", "werw", "erew@.com"));
        studentlist.add(new Student("abcd", "2222", "dsfs", "fgdf@.com"));

        // in ra studentsbygroup
        System.out.println("List student before removing:");
        studentsByGroup();

        //xoa hoc sinh co so id
        System.out.println("_______________________________");
        System.out.println("List student after removing");
        removeStudent();
        studentsByGroup();
        System.out.println("_______________________________");

        // check is sameGroup?
        System.out.println("Are stu1&stu2 same group? "+ sameGroup(stu1, stu2));
        System.out.println("Are stu2&stu3 same group? "+ sameGroup(stu2, stu3));
        System.out.println("Are stu1&stu3 same group? "+ sameGroup(stu1, stu3));
        System.out.println("Are stu1&stu4 same group? "+ sameGroup(stu1, stu4));

    }

    /**
     * ben duoi la ham kiem tra xem 2 sinh vien co cung lop
     * @param s1 sinh vien thu nhat
     * @param s2 sinh vien thu hai
     * @return true neu 2 sv cung lop, false neu khac lop
     */
    private static boolean sameGroup(Student s1, Student s2) {
        return (s1.getGroup().equals(s2.getGroup()));
    }

    /**
     * ben duoi la ham in ra danh sach sinh vien tung lop
     * @return ko tra ve gia tri nao
     */
    private static void studentsByGroup() {
        Vector<String> list = new Vector<String>();
        for(int i = 0; i < studentlist.size(); i++) {
            list.add(i, "stu" + i);
            System.out.println(list.indexOf(i));
        }
    }

    /**
     * ben duoi la ham de xoa sinh vien co id khoi list
     * @return void ko tra ve
     */
    private static void removeStudent() {
        for (int j = 0; j < studentlist.size(); j++) {
            if (studentlist.get(j).getId().equals("2222")) {
                studentlist.remove(j);
                j--;
            }
        }
    }

}