
/**
 * bai2 la ung dung sap xep noi bot
 * @author HaMinhHai
 * @version 1.8.2
 * @since 2018-11-13
 */
public class bai2 {

    /**
     * ham sap xep noi bot
     */
    public static void bubbleSort() {
        double[] array = new double[1000];

        for(int i = 0; i < 1000; i++) {
            array[i] = Math.random() * 1000;
        }

        for(int i = 0; i < 1000; i++) {

            for(int j = 1; j < 1000 - i; j++) {

                if(array[j-1] > array[j]) {

                    double tmp = array[j-1];
                    array[j - 1] = array[j];
                    array[j] = tmp;

                }
            }
        }

        for(int i = 0; i < 1000; i++) {
            System.out.println(array[i]);
        }
    }

    /**
     * ham main
     * @param args no use
     */
    public static void main(String[] args) {
        bubbleSort();
    }
}

