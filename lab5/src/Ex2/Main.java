package Ex2;

/**
 * kiem tra phuong thuc
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class Main {
    /**
     * main la ham khoi tao cac doi tuong can kiem tra
     * @param args ko su dung
     * @return main ko tra ve
     */
    public static void main(String[] args)
    {
        Circle c = new Circle("red",true,2.0);
        System.out.println(c.toString());
        c.CVDT();

        Rectangle hcn = new Rectangle("yellow",false,5.0,10);
        System.out.println(hcn.toString());

        Square hv = new Square("red",true,4.0,5.0,6.0);
        System.out.println(hv.toString());
    }
}
