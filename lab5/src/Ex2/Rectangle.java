package Ex2;

/**
 * Rectangle la lop chua cac thuoc tinh cua hinh chu nhat va
 * chua tat ca thuoc tinh cua lop Shape
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class Rectangle extends Shape
{
    /**
     * width la chieu rong hcn
     * length la chieu dai hcn
     */
    private double width=1.0,length=1.0;
    public Rectangle(String color, boolean filled, double width, double length) {
        super(color, filled);
        this.width=width;
        this.length=length;
    }
    /**
     * setWidth la ham setter de cai dat gia tri cho width va length
     * @param width bien dang double
     * @return void ko tra ve
     */
    void setWidth(double width) {
        this.width = width;
    }
    /**
     * setLength la ham setter de cai dat gia tri cho width va length
     * @param length bien dang double
     * @return void ko tra ve
     */
    void setLength(double length)
    {
        this.length = length;
    }
    /**
     * getWidth la ham getter de goi ra gia tri cua width
     * @return width
     */
    public double getWidth() {
        return width;
    }
    /**
     * getLength la ham getter de goi ra gia tri cua length
     * @return length
     */
    public double getLength() {
        return length;
    }
    public String toString()
    {
        return
                "\nHinh Chu Nhat \n"+
                        "Mau Sac:"+this.getColor()+"\n"+
                        "To mau: "+ this.getFilled() +"\n"+
                        "Chieu Dai:" +this.getLength()+"\n"+
                        "Chieu Rong:"+this.getWidth()+"\n";
    }
}