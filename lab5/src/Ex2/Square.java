package Ex2;

/**
 * Square la lop chua cac thuoc tinh cua 1 hinh vuong va
 * ke thua tat ca thuoc tinh cua lop Rectangle
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class Square extends Rectangle {
    /**
     * side la canh hinh vuong
     */
    public Square(String color, boolean filled, double width,double length,double side) {
        super(color, filled,width,length);
        super.setWidth(side);
        super.setLength(side);
    }

    /**
     * setWidth la ham setter de cai dat gia tri cho side
     *
     * @param side bien dang double
     * @return void ko tra ve
     */
    public void setWidth(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    /**
     * setLength la ham setter de cai dat gia tri cho side
     *
     * @param side bien dang double
     * @return void ko tra ve
     */
    public void setLength(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    /**
     * getSide la ham getter de goi ra gia tri cua side
     *
     * @return side
     */
    public double getSide() {
        return super.getLength();
    }

    public String toString() {
        return
                "\nHinh Vuong \n" +
                        "Mau Sac:" + this.getColor() + "\n" +
                        "To mau: " + this.getFilled() + "\n" +
                        "Canh hv:" + this.getSide() + "\n";
    }

}