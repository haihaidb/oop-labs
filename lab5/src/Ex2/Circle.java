package Ex2;

/**
 * Circle la lop chua cac thuoc tinh cua hinh tron va ke thua
 * cac thuoc tinh cua lop Shape
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class Circle extends Shape
{
    /**
     * radius la ban kinh hinh tron
     * pi la so pi = 3.14....
     */
    private double radius = 1.0;
    final double pi = Math.PI;

    public Circle(String color, boolean filled,double radius) {
        super(color, filled);
        this.radius=radius;
    }

    /**
     * setRadius la ham setter de cai dat gia tri cho radius
     * @param radius bien dang double
     * @return void ko tra ve
     */
    void setRadius(double radius) {
        this.radius = radius;
    }
    /**
     * getRadius la ham getter de goi ra gia tri cua radius
     * @return radius
     */
    private double getRadius() {
        return radius;
    }
    public String toString()
    {
        return
                "\nHinh Tron \n"+
                        "Mau Sac:"+this.getColor()+"\n"+
                        "To mau: "+ this.getFilled() +"\n"+
                        "Ban kinh:" +this.getRadius();
    }
    /**
     * CVDT la ham tinh chu vi va dien tich hinh tron
     */
    void CVDT()
    {
        double cv = (2*this.getRadius()*pi);
        double dt =(pi*Math.pow(this.getRadius(),2));
        System.out.println("Chu vi hinh tron la:"+cv);
        System.out.println("Dien tich hinh tron la:"+dt);
    }

}