package Ex2;

/**
 * Shape la lop gom cac thuoc tinh cha 1 hinh hoc nao do
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class Shape
{
    /**
     * color la mau sac cua hinh dc khoi tao bs gia tri ban dau
     * la mau do
     * filled la bien neu to mau se tra ve true
     */
    private String color = "red";
    private boolean filled = true;
    public Shape(String color,boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }
    /**
     * setColor la ham setter de cai dat gia tri cho color
     * @param color bien dang String
     * @return void ko tra ve
     */
    void setColor(String color) {
        this.color = color;
    }
    /**
     * setFilled la ham setter de cai dat gia tri cho filled
     * @param filled bien dang boolean
     * @return void ko tra ve
     */
    void setFilled(boolean filled) {
        this.filled = filled;
    }
    /**
     * getColor la ham getter de goi ra gia tri cua color
     * @return  color
     */
    String getColor() {
        return color;
    }
    /**
     * getFilled la ham getter de goi ra gia tri cua filled
     * @return filled
     */
    boolean getFilled() {
        return filled;
    }
}
