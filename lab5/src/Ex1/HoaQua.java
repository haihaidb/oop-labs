package Ex1;

/**
 * HoaQua la lop thuoc tinh cua 1 loai hoa qua nao do
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class HoaQua {
    /**
     * price la gia ban cua hoa qua (vnd)
     * amount la so luong mua cua hoa qua
     * place la xuat xu hoa qua
     * date la ngay nhap vao hoa qua
     */
    private int price, amount;
    private String place, date;

    /**
     * ham khoi tao gia tri cho cac bien
     * @param price la gia ban cua hoa qua (vnd)
     * @param amount la so luong mua cua hoa qua
     * @param place la xuat xu hoa qua
     * @param date la ngay nhap vao hoa qua
     */
    public HoaQua(int price,int amount,String place,String date)
    {
        this.price=price;
        this.amount=amount;
        this.date=date;
        this.place=place;
    }
    /**
     * setPrice la ham setter de cai dat gia tri cho price
     * @param price bien dang integer
     * @return void ko tra ve
     */
    void setPrice(int price) {
        this.price = price;
    }
    /**
     * setPlace la ham setter de cai dat gia tri cho place
     * @param place bien dang String
     * @return void ko tra ve
     */

    void setPlace(String place) {
        this.place = place;
    }
    /**
     * setAmount la ham setter de cai dat gia tri cho amount
     * @param amount bien dang integer
     * @return void ko tra ve
     */
    void setAmount(int amount) {
        this.amount = amount;
    }
    /**
     * setDate la ham setter de cai dat gia tri cho date
     * @param date bien dang String
     * @return void ko tra ve
     */
    void setDate(String date) {
        this.date = date;
    }
    /**
     * getDate la ham getter de goi ra gia tri cua date
     * @return  date
     */
    String getDate() {
        return date;
    }
    /**
     * getPrice la ham getter de goi ra gia tri cua price
     * @return  price
     */
    int getPrice() {
        return price;
    }
    /**
     * getAmount la ham getter de goi ra gia tri cua amount
     * @return amount
     */
    int getAmount() {
        return amount;
    }
    /**
     * getPlace la ham getter de goi ra gia tri cua place
     * @return place
     */
    String getPlace() {
        return place;
    }
}