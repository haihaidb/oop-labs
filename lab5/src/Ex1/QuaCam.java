package Ex1;

/**
 * lop QuaCam ke thua tat ca thuoc tinh cua lop HoaQua
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class QuaCam extends HoaQua {
    /**
     * ham khoi tao gia tri cho cac bien cua QuaCam
     *
     * @param price  la gia ban cua hoa qua (VND)
     * @param amount la so luong mua cua hoa qua
     * @param place  la xuat xu hoa qua
     * @param date   la ngay nhap vao hoa qua
     */
    public QuaCam(int price, int amount, String place, String date) {
        super(price,amount,place,date);


    }
    public String toString()
    {
        return
                "Qua Cam \n"+
                        "Gia ban theo can:"+this.getPrice()+"\n"+
                        "So luong mua: "+ this.getAmount() +"\n"+
                        "Xuat xu:" +this.getPlace()+"\n"+
                        "Ngay nhap:"+this.getDate()    +"\n";
    }
}