package Ex1;

/**
 * la ung dung de kiem tra
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */

public class Main {
    /**
     * main la ham khoi tao cac doi tuong can kiem tra
     * @param args ko su dung
     * @return main ko tra ve
     */
    public static void main(String[] args) {
        QuaTao apple = new QuaTao(15000,20,"HaiPhong","25-9-2018");
        System.out.println(apple.toString());
        System.out.println("___________________________");

        QuaCam orange = new QuaCam(20000,15,"DienBien","18-9-2018");
        System.out.println(orange.toString());
        System.out.println("___________________________");

        CamCaoPhong cp = new CamCaoPhong(21000,15,"SonLa","30-9-2018");
        System.out.println(cp.toString());
        System.out.println("___________________________");

        CamSanh cs = new CamSanh(30000,30,"HaNoi","1-9-2018");
        System.out.println(cs.toString());



    }
}
