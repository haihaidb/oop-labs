package Ex1;

/**
 * lop QuaTao ke thua tat ca thuoc tinh cua lop HoaQua
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class QuaTao extends HoaQua {

    /**
     * ham khoi tao gia tri cho cac bien cua QuaTao
     *
     * @param price  la gia ban cua hoa qua (vnd)
     * @param amount la so luong mua cua hoa qua
     * @param place  la xuat xu hoa qua
     * @param date   la ngay nhap vao hoa qua
     */
    public QuaTao(int price, int amount, String place, String date) {
        super(price, amount, place, date);
    }
    public String toString()
    {
        return
                "Qua Tao \n"+
                        "Gia ban theo can:"+this.getPrice()+"\n"+
                        "So luong mua: "+ this.getAmount() +"\n"+
                        "Xuat xu:" +this.getPlace()+"\n"+
                        "Ngay nhap:"+this.getDate()    +"\n";
    }
}