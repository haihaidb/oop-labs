package Ex1;

/**
 * lop CamCaoPhong ke thua tat ca cac thuoc tinh cua lop QuaCam
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-02
 */
public class CamCaoPhong extends QuaCam {


    /**
     * ham khoi tao gia tri cho cac bien cua CamCaoPhong
     *
     * @param price  la gia ban cua hoa qua (vnd)
     * @param amount la so luong mua cua hoa qua
     * @param place  la xuat xu hoa qua
     * @param date   la ngay nhap vao hoa qua
     */
    public CamCaoPhong(int price, int amount, String place, String date) {
        super(price, amount, place, date);
    }
    public String toString()
    {
        return
                "Cam Cao Phong \n"+
                        "Gia ban theo can:"+this.getPrice()+"\n"+
                        "So luong mua: "+ this.getAmount() +"\n"+
                        "Xuat xu:" +this.getPlace()+"\n"+
                        "Ngay nhap:"+this.getDate()    +"\n";
    }
}