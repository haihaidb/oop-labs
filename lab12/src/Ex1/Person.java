package Ex1;
/**
 * Person class
 * @author Ha Minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */
public abstract class Person
{
    protected String sex,name, birthday;
    /**
     * Constructor
     * @param sex gioi tinh
     * @param name ten
     * @param birthday ngay thang nam sinh
     */
    public Person(String name, String birthday, String sex)
    {
        this.sex = sex;
        this.name = name;
        this.birthday = birthday;
    }
    /**
     * Ham in gia pha
     * @param deep do sau cua nut cha
     */
    public abstract void show(int deep);
}

