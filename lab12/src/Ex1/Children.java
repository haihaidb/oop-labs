package Ex1;
/**
 * Lop con chau
 * @author Ha Minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */


import java.util.Arrays;

public class Children extends Person
{
    /**
     * constructor
     * @param name ten
     * @param birthday ngay thang nam sinh
     * @param sex gioi tinh
     */
    public Children(String name, String birthday, String sex)
    {
        super(name, birthday, sex);
    }

    /**
     * Ham in pha he
     * @param deep do sau
     */
    @Override
    public void show(int deep)
    {
        char[] charater = new char[deep];

        String s = new String(charater);
        System.out.println(s + super.name);
    }
}
