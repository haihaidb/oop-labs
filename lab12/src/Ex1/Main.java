package Ex1;

import java.util.LinkedList;

/**
 * class Main kiem tra
 * @author Ha Minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */
public class Main
{
    /**
     * Ham in ra nhung nguoi doc than
     * @param person nut cha
     */
    private static void printAlone(Person person)
    {
        if (person instanceof Children || (person instanceof Parents && ((Parents) person).list.isEmpty()))
        {
            System.out.println(person.name);

            return;
        }
        LinkedList<Person> list = ((Parents) person).list;
        int i = 0, listSize = list.size();
        while (i < listSize) {
            Person person1 = list.get(i);
            printAlone(person1);
            i++;
        }
    }

    /**
     * Ham in cap vo chong co 2 con
     * @param person nut cha
     */
    private static void printDoubleChild(Person person)
    {
        if (person instanceof Children || (person instanceof Parents && ((Parents) person).list.isEmpty())) return;
        if(((Parents) person).list.size() == 2)
            System.out.println(person.name + " <3 " +((Parents) person).marriedWith);
        LinkedList<Person> list = ((Parents) person).list;
        int i = 0, listSize = list.size();
        while (i < listSize) {
            Person person1 = list.get(i);
            printDoubleChild(person1);
            i++;
        }
    }

    /**
     * Ham in the he moi nhat
     * @param person nut goc
     * @param newest check xem nut hien tai co kha nang lam nut moi nhat ko(F: chac chan ko, T: co the co)
     */
    private static void printNewest(Person person, boolean newest)
    {
        boolean checkcapable = true;

        if(person instanceof Children)
        {
            if (newest)
                System.out.println(person.name);

            return;
        }

        LinkedList<Person> list = ((Parents) person).list;
        int i = 0, listSize = list.size();
        while (i < listSize) {
            Person person1 = list.get(i);
            if (person1 instanceof Parents && ((Parents) person1).list.size() > 0)
                checkcapable = false;
            i++;
        }

        LinkedList<Person> list1 = ((Parents) person).list;
        int j = 0, list1Size = list1.size();
        while (j < list1Size) {
            Person person1 = list1.get(j);
            printNewest(person1, checkcapable);
            j++;
        }


    }

    public static void main(String[] args)
    {
        Parents grandmother = new Parents("Hanna", "7/3/1959", "Female", "James");
        grandmother.addChildren(new Children("Ryan", "28/9/1980","Male"));

        Parents daddy = new Parents("Kai","8/5/1986","Male","Jennifer");
        grandmother.addChildren(daddy);

        Parents firstchild = new Parents("Justin", "21/9/2015","Male","Gomez");
        firstchild.addChildren(new Children("Selebier","14/4/2044","Female"));
        firstchild.addChildren(new Children("Ben", "18/8/2048", "Male"));
        daddy.addChildren(firstchild);

        Parents secondchild = new Parents("Mark", "28/2/2020","Male","Ariana");
        secondchild.addChildren(new Children("Linda","8/6/2050","Female"));
        daddy.addChildren(secondchild);

        Parents thirdchild = new Parents("Taylor", "11/9/2022","Female","Charlie");
        thirdchild.addChildren(new Children("Elyza","29/1/2040","Female"));
        thirdchild.addChildren(new Children("David","30/12/2046","Male"));
        daddy.addChildren(thirdchild);

        Parents fourthchild = new Parents("Sarah", "3/11/2025","Female","Paul");
        fourthchild.addChildren(new Children("Puth","8/2/2047","Male"));
        daddy.addChildren(fourthchild);

        grandmother.show(0);

        System.out.println("----Alone:");
        printAlone(grandmother);

        System.out.println("----Family bores two children:");
        printDoubleChild(grandmother);

        System.out.println("----Newest generation:");
        printNewest(grandmother,true);
    }
}
