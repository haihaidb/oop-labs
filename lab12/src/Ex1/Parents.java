package Ex1;

/**
 * lop cha me
 * @author Ha minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */

import java.util.LinkedList;
import java.util.Arrays;


public class Parents extends Person
{
    protected String marriedWith;
    protected LinkedList<Person> list = new LinkedList<Person>();
    /**
     * constructor
     * @param name ten
     * @param birthday ngay thang nam sinh
     * @param sex gioi tinh
     */
    public Parents(String name, String birthday, String sex)
    {
        super(name, birthday, sex);
    }
    /**
     * constructor
     * @param name ten
     * @param birthday ngay thang nam sinh
     * @param sex gioi tinh
     * @param marriedWith male/female
     */
    public Parents(String name, String birthday, String sex, String marriedWith)
    {
        super(name, birthday, sex);
        this.marriedWith = marriedWith;
    }

    /**
     * Ham them mot con
     * @param p person
     */
    public void addChildren(Person p)
    {
        list.add(p);
    }
    /**
     * Ham in pha he
     * @param deep do sau
     */
    @Override
    public void show(int deep) {
        char[] charater = new char[deep];
        String s = new String(charater);
        System.out.println(s + super.name);

        int i = 0, listSize = list.size();
        while (i < listSize) {
            Person person = list.get(i);
            person.show(deep + 1);
            i++;
        }
    }

}
