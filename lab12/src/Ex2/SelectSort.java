package Ex2;

/**
 * class SelectSort cai dat ham sap xep chon
 * @author Ha Minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */
public class SelectSort implements SortAlgorithm
{

    /**
     * Cai dat sap xep chon
     * @param a mang cac so nguyen
     * @param increasing true/false sort tang dan/giam dan
     * @return sorted array
     */
    @Override
    public int[] sort(int[] a, boolean increasing)
    {
        for(int i = 0; i < a.length; i++)
        {
            int m = i;
            for(int j = i+1; j < a.length; j++)
                if((a[m]>a[j]) == increasing) m = j;

            int temp = a[i];
            a[i] = a[m];
            a[m] = temp;
        }

        return a;
    }
}
