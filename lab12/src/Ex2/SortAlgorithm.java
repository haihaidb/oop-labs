package Ex2;

/**
 * interface SortAlgorithm trong do co ham sort
 * @author Ha Minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */
public interface SortAlgorithm
{
    /**
     * Ham sort
     * @param a mang cac so nguyen
     * @param n so luong phan tu trong mang
     * @param increasing true/false sort tang dan/giam dan
     * @return sorted array
     */
    int[] sort(int[] a, boolean increasing);
}