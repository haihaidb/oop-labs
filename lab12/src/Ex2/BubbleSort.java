package Ex2;

/**
 * class BubbleSort cai dat ham sort
 * @author Ha Minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */
public class BubbleSort implements SortAlgorithm
{
    /**
     * Cai dat sap xep noi bot
     * @param a mang so nguyen
     * @param increasing True/False sort tang dan/giam dan
     * @return sorted array
     */
    @Override
    public int[] sort(int[] a, boolean increasing)
    {
        for(int i = 0; i < a.length; i++)
        {
            for(int j = i+1; j < a.length; j++)
                if((a[i]>a[j]) == increasing)
                {
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
        }
        return a;
    }
}
