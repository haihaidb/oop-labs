package Ex2;

/**
 * Main class
 * @author Ha Minh Hai
 * @version 1.8.1
 * @since 2018-12-3
 */
public class Main
{
    public static void main(String[] args)
    {
        int a[] = {6,1,4,3,9,15,11,54,23,30};

        SortAlgorithm bubbleSort = new BubbleSort();
        SortAlgorithm selectSort = new SelectSort();

        System.out.print("Before sort: ");
        print(a);
        //a = selectSort.sort(a, false);
        a = bubbleSort.sort(a, false);
        System.out.print("After sort: ");
        print(a);
    }
    private static void print(int a[])
    {
        for (int i : a) System.out.print(i + " ");
        System.out.println();
    }

}
