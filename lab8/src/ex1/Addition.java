/**
 * Addition la phuong thuc phep cong 2 so nguyen
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;

/**
 * ham constructor
 */
public class Addition extends BinaryExpression {
    private Expression left,right;
    public Addition(Expression left,Expression right) {
        super(left,right);
        this.left=left;
        this.right=right;
    }
    /*
     * overriding getter
     */

    @Override
    public Expression getLeft() {
        return this.left;
    }
    /*
    overriding getter
     */
    @Override
    public Expression getRight() {
        return this.right;
    }
    /*
    overriding toString cong 2 so nguyen
     */
    @Override
    public String toString() {
        int sum = getLeft().evaluate() + getRight().evaluate();
        return "";
    }
    /*
    overriding evaluate kq cong 2 nguyen
     */
    @Override
    public int evaluate() {
        int sum =  getLeft().evaluate() + getRight().evaluate();
        return sum;
    }


}
