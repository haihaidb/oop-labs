/**
 * Division la phuong thuc chia 2 so nguyen
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;

public class Division extends BinaryExpression {

    /**
     * hàm constructor khởi tạo 2 số nguyên
     */
    private Expression left,right;
    public Division(Expression left,Expression right) {
        super(left,right);
        this.left=left;
        this.right=right;
    }

    //overriding getLeft
    @Override
    public Expression getLeft() {
        return this.left;
    }
    //overriding getRight
    @Override
    public Expression getRight() {
        return this.right;
    }
    //overriding toString: thuong 2 so nguyen
    @Override
    public String toString() {
        int div = getLeft().evaluate() / getRight().evaluate();
        return "";
    }
    //overriding evaluate thuong 2 so nguyen
    @Override
    public int evaluate() {
        int div = getLeft().evaluate() / getRight().evaluate();;
        return div;
    }


}
