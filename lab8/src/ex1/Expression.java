
/**
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;
abstract public class Expression {
    // truu tuong
    abstract public String toString();
    abstract public int evaluate();
}
