/**
 * Square la phuong thuc binh phuong 1 so
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;


public class Square extends Expression {
    private Expression expression;
    /**
     * hàm constructor khởi tạo 1 số nguyên
     */
    public Square(Expression expression)
    {
        this.expression=expression;
    }

    /**
     * getter
     * @return expression
     */
    public Expression getExpression() {
        return expression;
    }

    /**
     * setter
     * @param expression bien tru tuong
     */
    public void setExpression(Expression expression) {
        this.expression = expression;
    }
    /*
    overriding toString binh phuong 1 so
     */
    @Override
    public String toString() {
        int result = getExpression().evaluate() * getExpression().evaluate();
        return "";
    }
    /*
    overriding evaluate binh phuong 1 so
     */
    @Override
    public int evaluate() {
        int result = getExpression().evaluate() * getExpression().evaluate();;
        return result;
    }
}
