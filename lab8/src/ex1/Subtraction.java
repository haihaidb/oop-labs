/**
 * Subtraction la phuong thuc phep tru 2 so nguyen
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;

/**
 * hàm constructor khởi tạo 2 số nguyên
 */
public class Subtraction extends BinaryExpression {
    private Expression left,right;
    public Subtraction(Expression left,Expression right) {
        super(left,right);
        this.left=left;
        this.right=right;
    }

    @Override
    public Expression getLeft() {
        return this.left;
    }

    @Override
    public Expression getRight() {
        return this.right;
    }

    @Override
    public String toString()
    {
        int sub = getLeft().evaluate() - getRight().evaluate();
        return "";
    }

    @Override
    public int evaluate()
    {
        int sub = getLeft().evaluate() - getRight().evaluate();
        return sub;
    }
}
