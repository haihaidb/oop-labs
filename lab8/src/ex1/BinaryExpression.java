/**
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;

/**
 * hàm constructor khởi tạo 2 số nguyên
 */

abstract public class BinaryExpression extends Expression{
    private Expression left,right;
    BinaryExpression(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }

    /**
     * getter tra ve gia tri so ben trai
     */
    abstract public Expression getLeft();

    /**
     * setter
     * @param left so ben trai
     */
    public void setLeft(Expression left) {
        this.left = left;
    }
    /**
     * getter tra ve gia tri so ben phai
     */
    abstract public Expression getRight() ;
    /**
     * setter
     * @param right so ben phai
     */
    public void setRight(Expression right) {
        this.right = right;
    }
    abstract public String toString();
    abstract public int evaluate();
}
