/**
 * Numeral la phuong thuc khoi tao 1 so nguyen
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;

public class Numeral extends Expression {
    private int value;

    /**
     * Ham khoi tao 1 so nguyen
     * @param value gia tri so nguyen
     */
    public Numeral(int value)
    {
        this.value=value;
    }
    /*
    getter
     */

    public int getValue() {
        return value;
    }
    /*
    setter
     */
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return getValue() + "";
    }

    @Override
    public int evaluate() {
        return getValue();
    }
}
