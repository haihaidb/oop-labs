/**
 * ExpresstionTest la lop thuc thi cac yeu cau phep tinh
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;

public class ExpresstionTest {
    public static void main(String[] args) throws ArithmeticException {
        /*
         * khoi tao cac so nguyen
         */
        Expression n1 = new Numeral(10);
        Expression n2 = new Numeral(1);
        Expression n3 = new Numeral(2);
        Expression n4 = new Numeral(3);
        Expression n5 = new Numeral(0);
        // cat dat cho phuong thuc (10^2-1+2*3)^2
        Expression res1 = new Square(n1);
        Expression res2 = new Multiplication(n3,n4);
        Expression res3 = new Subtraction(res1,n2);
        Expression res4 = new Addition(res3,res2 );
        Expression res  = new Square(res4);

        System.out.println(res.evaluate());
        try {
            Expression div = new Division(n1, n5);
            System.out.println(div);
        }catch (ArithmeticException e)
        {
            System.out.println("Loi chia cho 0!");
        }
    }
}
