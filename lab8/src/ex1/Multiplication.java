/**
 * Multiplication la phuong thuc nhan 2 so nguyen
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
package ex1;

/**
 * hàm constructor khởi tạo 2 số nguyên
 */
public class Multiplication extends BinaryExpression {
    private Expression left,right;
    public Multiplication(Expression left,Expression right) {
        super(left,right);
        this.left=left;
        this.right=right;
    }
    //overriding getLeft
    @Override
    public Expression getLeft() {
        return this.left;
    }
    //overriding getRight
    @Override
    public Expression getRight() {
        return this.right;
    }
    //overriding toString tich 2 so nguyen
    @Override
    public String toString() {
        int mul = getLeft().evaluate() * getRight().evaluate();
        return "";
    }
    //overriding evaluate tich 2 so nguyen
    @Override
    public int evaluate() {
        int mul = getLeft().evaluate() * getRight().evaluate();
        return mul;
    }


}
