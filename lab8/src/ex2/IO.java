package ex2; /**
 * la lop cai dat truong hop ngoai le IOExeption
 * @author Ha Minh Hai
 * @version 1.8.2;
 * @since 2018-10-23
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class IO {
    /**
     * duong dan toi file
     */

    public static void main(String[] args) {

        try {
            File f = new File("ok.txt");
            BufferedReader reader = new BufferedReader(new FileReader(f));
        } catch(java.io.IOException e) {
            System.out.println("Loi IOException");
        }

    }

}
