package ex2; /**
 * la lop cai dat truong hop ngoai le  FileNotFoundException
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
import java.io.File;
import java.util.Scanner;

public class FilenotFound{
    public static void file() throws java.io.FileNotFoundException
    {
        Scanner input = new Scanner(new File("text.txt"));
            String in = "";
            in = input.nextLine();
        //throw new FileNotFoundException("Loi ko tim thay file ");
    }
    public static void main(String[] args) {
        try
        {
            file();
        }
        catch (java.io.FileNotFoundException e)
        {
            System.out.println("Loi ko tim thay file!");
        }

    }

}
