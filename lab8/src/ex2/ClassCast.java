package ex2;
/**
 * La lop cai dat truong hop ngoai le ClassCastExeption
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */

import java.lang.ClassCastException;

public class ClassCast {
    public static void main(String[] args) throws ClassCastException {

        try {
            Object i = 60;
            String s = (String) i;
        }
        catch (ClassCastException e)
        {
            System.out.println("Loi ep kieu class!");
        }

    }
}
