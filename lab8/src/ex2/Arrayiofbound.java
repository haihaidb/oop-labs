package ex2;

/**
 * la lop cai dat truong hop ngoai le  ArrayIndexOutOfBoundsException
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
public class Arrayiofbound {
    public static void main(String[] args) throws ArrayIndexOutOfBoundsException {
        int a[] = new int[99];
        try
        {
            a[100]=0;
            System.out.println(a[100]);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println("Loi tran mang");
        }
    }
}
