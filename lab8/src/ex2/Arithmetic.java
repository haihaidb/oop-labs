package ex2;
/**
 * la lop cai dat truong hop ngoai le ArithmeticExeption
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */

public class Arithmetic {
    public static void main(String[] args) throws ArithmeticException{
        int b=0;
        try{
            int a=1999/b;
            System.out.println(a);
        }
        catch (ArithmeticException e)
        {
            System.out.println("Loi chia chia cho 0");
        }
    }
}
