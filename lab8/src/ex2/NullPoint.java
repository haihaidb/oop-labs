 /**
 * la lop cai dat truong hop ngoai le  NullPointerException
 * @author Hà Minh Hải
 * @version 1.8.2;
 * @since 2018-10-23
 */
 package ex2;

public class NullPoint {
    public static void main(String[] args) throws NullPointerException {
        try
        {
            Object str = null;
            str.hashCode();
        }
        catch (NullPointerException e) {
            System.out.println("Type of parameter can't equal null");
        }
        }
}

