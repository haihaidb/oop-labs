/**
 * Day la ung dung de cong tru nhan chia 2 phan so
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-09-22
 */

public class Ex2 {

    /**
     * main la ham de thuc thi cac phep cong tru nhan chia
     * @param args khong su dung
     * @return main ko tra ve gia tri nao
     */

    public static void main(String[] args){
        PS a = new PS(15,10);
        PS b = new PS(15,10);
        a.congPS(b);
        a.truPS(b);
        a.nhanPS(b);
        a.chiaPS(b);
        System.out.println("Are a&b equals? " + a.equals(b));

    }


    static class PS{
        int tu,mau;
        /**
         * PS day la phuong thuc khoi tao phan so
         * @param tu la tu so
         * @param mau la mau so
         * @return ko tra ve gia tri nao
         */
        PS(int tu, int mau){
            this.tu=tu;
            this.mau=mau;
        }

        /**
         * getTu la ham lay ra gia tri tu so
         * @return gia tri tu so
         */
        int getTu(){
            return tu;
        }

        /**
         * setTu la ham truyen vao gia tri tu so
         * @param tu la tu so cua phan so
         * @return void ko tra ve gia tri nao
         */
        void setTu(int tu)
        {
            this.tu=tu;
        }

        /**
         * getMau la ham lay ra gia tri mau so
         * @return gia tri mau so
         */
        int getMau(){
            return mau;
        }

        /**
         * setMau la ham truyen vao gia tri mau so
         * @param mau la mau so cua phan so
         * @return void ko tra ve gia tri nao
         */
        void setMau(int mau) {
            this.mau = mau;
        }

        /**
         * result la in ra cac ket qua cua cac phep cong tru nhan chia
         * 2 phan so
         * @return ket qua cac phep cong tru nhan chia 2 phan so
         */
        String result(){
            if((this.getTu()<0 && this.getMau()<0)||(this.getTu()>0 && this.getMau()<0)) {
                return this.getTu() * -1 + "/" + this.getMau()*-1;
            }
            if(this.getMau()==0){
                return "invalid!";
            }

            else if(this.getMau()==1){
                return this.getTu() +"";
            }
            return this.getTu() +"/"+this.getMau();
        }

        /**
         * congPS la phuong thuc cong 2 phan so
         * @param ps la bien thuoc lop PS
         * @return void ko tra ve gia tri nao
         */
        void congPS(PS ps){
            int ts=this.getTu() * ps.getMau() + ps.getTu() *  this.getMau();
            int ms=this.getMau() * ps.getMau();
            PS pstong = new PS(ts,ms);
            pstong.PSTG();
            System.out.println("Tong 2 PS la : " +pstong.result());
        }

        /**
         * truPS la phuong thuc tru 2 phan so
         * @param ps la bien thuoc lop PS
         * @return void ko tra ve gia tri nao
         */
        void truPS(PS ps){
            int ts=this.getTu() * ps.getMau() - ps.getTu() *  this.getMau();
            int ms=this.getMau() * ps.getMau();
            PS pshieu = new PS(ts,ms);
            pshieu.PSTG();
            System.out.println("Hieu 2 PS la : " + pshieu.result());
        }
        /**
         * nhanPS la phuong thuc nhan 2 phan so
         * @param ps la bien thuoc lop PS
         * @return void ko tra ve gia tri nao
         */
        void nhanPS(PS ps){
            int ts=this.getTu() * ps.getTu();
            int ms=this.getMau() * ps.getMau();
            PS pstich = new PS(ts,ms);
            pstich.PSTG();
            System.out.println("Tich 2 PS la : " + pstich.result());
        }
        /**
         * chiaPS la phuong thuc chia 2 phan so
         * @param ps la bien thuoc lop PS
         * @return void ko tra ve gia tri nao
         */
        void chiaPS(PS ps){
            int ts=this.getTu() * ps.getMau();
            int ms=this.getMau() * ps.getTu();
            PS psthuong = new PS(ts,ms);
            psthuong.PSTG();
            System.out.println("Thuong 2 PS la : " + psthuong.result());
        }
        /**
         * UCLN la phuong thuc tim ra uoc chung lon nhat cua tu va mau so
         * @param a la tu so
         * @param b la mau so
         * @return gia tri uoc chung lon nhat cua a va b
         */
        static int UCLN(int a, int b) {
            a = Math.abs(a);
            b = Math.abs(b);
            if (a == 0 || b == 0)
                return a + b;
            while (a != b) {
                if (a > b)
                    a = a - b;
                else
                    b = b - a;
            }
            return a;
        }

        /**
         * PSTG la ham de dua phan so ve dang toi gian
         * @return void ko tra ve gia tri nao
         */
        void PSTG(){
            int ucln = UCLN(tu,mau);
            this.setTu(this.getTu()/ucln);
            this.setMau(this.getMau()/ucln);

        }

        /**
         * equals la ham so sanh 2 phan so bang nhau
         * @param obj doi tuong can so sanh
         * @return true neu 2 phan so bang nhau
         *         false neu 2 phan so khac nhau
         */
        boolean equals(PS obj){
            this.PSTG();
            obj.PSTG();
            return Math.abs(this.getTu()) == Math.abs(obj.getTu()) &&
                    Math.abs(this.getMau()) == Math.abs(obj.getMau());

        }

    }
}
