package Ex3;

/**
 * nuocgiaikhat la class mieu ta dac diem cua chai nuoc
 * giai khat
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Drink{
    String loai;
    int price;
    /**
     * khoi tao cac dac diem cho chai nuoc giai khat
     * @param loai la loai nuoc cua hang nao
     * @param price la gia cua chai nuoc
     * @return ko tra ve gia tri nao
     */
    public Drink (String loai,int price){
        this.loai=loai;
        this.price=price;
    }
    /**
     * duoi day la phuong thuc lay ra ten loai nuoc
     * giai khat
     * @return loai
     */
    public String getLoai(){
        return loai;
    }
    /**
     * duoi day la phuong thuc cai dat loai nuoc
     * giai khat
     * @param  loai co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setLoai(String loai){
        this.loai=loai;
    }
    /**
     * duoi day la phuong thuc lay ra gia cua nuoc
     * @return price
     */
    public int getPrice(){
        return price;
    }
    /**
     * duoi day la phuong thuc cai dat gia cua nuoc
     * @param  price co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setPrice(int price){
        this.price=price;
    }
    /**
     * in ra thuoc tinh
     */
    public void print() {
        System.out.println("The " + getLoai() + "'s price is " + getPrice() + "VND");
    }

}