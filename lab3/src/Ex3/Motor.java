package Ex3;

/**
 * motor la class mieu ta dac diem cua xe may
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Motor{
    int phankhoi;
    String hangxe,loaixe;
    /**
     * khoi tao cac dac diem cho xe may
     * @param phankhoi la so phan khoi cua xe may
     * @param hangxe la ten hang xe cua xe may
     * @param loaixe la loai xe co the la xe ga,xe so,xe con
     * @return ko tra ve gia tri nao
     */
    public Motor(int phankhoi,String hangxe,String loaixe){
        this.phankhoi=phankhoi;
        this.hangxe=hangxe;
        this.loaixe=loaixe;
    }
    /**
     * duoi day la phuong thuc lay ra phan khoi cua xe
     * @return phankhoi
     */
    public int getPhankhoi(){
        return phankhoi;
    }
    /**
     * duoi day la phuong thuc cai dat so phan khoi cho xe
     * @param  phankhoi co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setPhankhoi(int phankhoi){
        this.phankhoi=phankhoi;
    }
    /**
     * duoi day la phuong thuc lay ra ten hang cua xe
     * @return hangxe
     */
    public String getHangxe(){
        return hangxe;
    }
    /**
     * duoi day la phuong thuc cai dat ten hang xe
     * @param  hangxe co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setHangxe(String hangxe){
        this.hangxe=hangxe;
    }
    /**
     * duoi day la phuong thuc lay ra loai xe
     * @return loai
     */
    public String getLoaixe(){
        return loaixe;
    }
    /**
     * duoi day la phuong thuc cai dat loai xe
     * @param  loaixe co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setLoaixe(String loaixe){
        this.loaixe=loaixe;
    }
    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println("Xe nay cua hang " + getHangxe() +", la loai xe " + getLoaixe() + " va co so phan khoi: " + getPhankhoi());
    }
}
