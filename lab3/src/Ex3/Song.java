package Ex3;

/**
 * song la class mieu ta dac diem cua bai hat
 */
public class Song{
    String name,theloai,quocgia;
    /**
     * khoi tao cac dac diem cho bai hat
     * @param name la ten bai hat
     * @param theloai la loai nhac cua bai hat
     * @param quocgia la bai hat thuoc quoc gia nao
     * @return ko tra ve gia tri nao
     */
    public Song (String name,String theloai,String quocgia){
        this.name=name;
        this.theloai=theloai;
        this.quocgia=quocgia;
    }
    /**
     * duoi day la phuong thuc lay ra ten bai hat
     * @return name
     */
    public String getName(){
        return name;
    }
    /**
     * duoi day la phuong thuc cai dat ten cho bai hat
     * @param  name co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setName(String name){
        this.name=name;
    }
    /**
     * duoi day la phuong thuc lay ra the loai nhac
     * @return theloai
     */
    public String getTheloai(){
        return theloai;
    }
    /**
     * duoi day la phuong thuc cai dat the loai cho bai hat
     * @param  theloai co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setTheloai(String theloai){
        this.theloai=theloai;
    }
    /**
     * duoi day la phuong thuc lay ra quoc gia cua bai hat
     * @return quocgia
     */
    public String getQuocgia(){
        return quocgia;
    }
    /**
     * duoi day la phuong thuc cai dat quoc gia cho bai hat
     * @param  quocgia co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setQuocgia(String quocgia){
        this.quocgia=quocgia;
    }

    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println(getName() + " is " + getTheloai() + " music and is produced in " + getQuocgia());
    }
}