package Ex3;

/**
 * Main thuc hien khoi tao cac doi tuong
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Main {
    public static void main(String[] args) {
        Building bd = new Building(2, "Lotte");
        Bus bus = new Bus(1,10000);
        Cat cat = new Cat("Meow","Red");
        Drink drink = new Drink("Cocacola", 10000);
        Fish fish = new Fish("Ca chep", "Ao ho");
        Hocsinh hs = new Hocsinh("12A2","Black & White", "THPT chuyen Le Quy Don");
        Lecturer gv = new Lecturer(30, "OOP","Male");
        Motor mt = new Motor(120, "Toyota", "Ga");
        Sinhvien sv = new Sinhvien("HaMinhHai", 17021238, "UET");
        Song s = new Song("Toi yeu Viet Nam", "Rock", "Vietnam");

        bd.print();
        bus.print();
        cat.print();
        drink.print();
        fish.print();
        hs.print();
        gv.print();
        mt.print();
        sv.print();
        s.print();

    }
}
