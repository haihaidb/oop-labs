package Ex3;

/**
 * Fish la class mieu ta dac diem cua con ca
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Fish {
    private String loai,noisong;
    /**
     * khoi tao cac dac diem cho con ca
     * @param loai la ten loai ca(vd:ca chep,ca voi,..)
     * @param noisong la noi song cua Fish(vd:ao,ho,song,bien,..)
     * @return ko tra ve gia tri nao
     */
    public Fish(String loai, String noisong){
        this.loai=loai;
        this.noisong=noisong;
    }
    /**
     * duoi day la phuong thuc lay ra ten loai ca
     * @return loai
     */
    public String getLoai(){
        return loai;
    }
    /**
     * duoi day la phuong thuc cai dat ten loai ca
     * @param  loai co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setLoai(String loai){
        this.loai=loai;
    }
    /**
     * duoi day la phuong thuc lay ra noi song cua ca
     * @return noisong
     */
    public String getNoisong(){
        return noisong;
    }
    /**
     * duoi day la phuong thuc cai dat noi song cho ca
     * @param  noisong co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setNoisong(String noisong){
        this.noisong=noisong;
    }
    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println(getLoai() + " song o " + getNoisong());
    }
}