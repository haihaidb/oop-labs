package Ex3;

/**
 * Lecturer la class mieu ta dac diem cua giang vien
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Lecturer {
    private int age;
    private String subject,sex;

    /**
     * khoi tao cac dac diem cho giang vien
     * @param age tuoi cua giang vien
     * @param subject mon hoc cua giang vien
     * @param sex gioi tinh cua giang vien
     * @return ko tra ve gia tri nao
     */
    public Lecturer(int age, String subject, String sex){
        this.age=age;
        this.subject=subject;
        this.sex=sex;
    }
    /**
     * getAge la phuong thuc de lay ra so tuoi cua giang vien
     * @return age
     */
    public int getAge(){
        return age;
    }
    /**
     * setAge la phuong thuc de cai dat so tuoi cho giang vien
     * @param age tuoi
     * @return void ko tra ve
     */
    public void setAge(int age){
        this.age=age;
    }
    /**
     * getSubject la phuong thuc de lay ra ten mon hoc cua
     * giang vien
     * @return subject
     */
    public String getSubject(){
        return subject;
    }
    /**
     * setSubject la phuong thuc de cai dat mon hoc cua
     * giang vien
     * @param subject mon hoc
     * @return void ko tra ve
     */
    public void setSubject(String subject){
        this.subject=subject;
    }
    /**
     * getSex la phuong thuc de lay ra gioi tinh cua
     * giang vien
     * @return sex
     */
    public String getSex(){
        return sex;
    }
    /**
     * setSex la phuong thuc de cai dat gioi tinh cua
     * giang vien
     * @param sex gioi tinh
     * @return void ko tra ve
     */
    public void setSex(String sex){
        this.sex=sex;
    }
    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println("This lecturer is " + getAge() + "," + getSex() + " and teachs " + getSubject());
    }

}