package Ex3;

/**
 * xebus la class mieu ta dac diem cua xe bus
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Bus{
    int number,price;
    /**
     * khoi tao cac dac diem cua xe bus
     * @param number la so xe cua xe bus
     * @param price la gia cua ve xe bus
     * @return ko tra ve gia tri nao
     */
    public Bus (int number,int price){
        this.number=number;
        this.price=price;
    }
    /**
     * duoi day la phuong thuc lay ra so cua xe bus
     * @return number
     */
    public int getNumber(){
        return number;
    }
    /**
     * duoi day la phuong thuc cai dat so cho xe bus
     * @param  number co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setNumber(int number){
        this.number=number;
    }
    /**
     * duoi day la phuong thuc lay ra gia ve xe bus
     * @return price
     */
    public int getPrice(){
        return price;
    }
    /**
     * duoi day la phuong thuc cai dat gia ve xe bus
     * @param  price co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setPrice(int price){
        this.price=price;
    }

    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println("The "+getNumber() + "th bus's price is " + getPrice()+ "VND");
    }
}