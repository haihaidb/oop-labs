package Ex3;

/**
 * Sinhvien la class mieu ta dac diem cua sinh vien
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Sinhvien {
    private String name, university;
    private int id;
    /**
     * khoi tao cac dac diem cho sinh vien
     * @param name la ten sinh vien
     * @param id la ma sinh vien cua sinh vien
     * @param university la ten truong dai hoc cua sinh vien
     * @return ko tra ve gia tri nao
     */
    public Sinhvien(String name, int id, String university){
        this.name=name;
        this.id =id;
        this.university = university;
    }
    /**
     * getName la phuong thuc lay ra ten cua sinh vien
     * @return name
     */
    public String getName(){
        return name;
    }

    /**
     * setName la phuong thuc cai dat ten cho sinh vien
     * @param name co dang string
     * @return void ko tra ve
     */
    public void setName(String name){
        this.name=name;
    }
    /**
     * getId la phuong thuc lay ra msv cua sinh vien
     * @return id
     */
    public int getId(){
        return id;
    }
    /**
     * setId la phuong thuc cai dat msv cho sinh vien
     * @param id co dang interger
     * @return void ko tra ve
     */
    public void setId(int id){
        this.id=id;
    }
    /**
     * getUniversity la phuong thuc lay ra ten truong cua sinh vien
     * @return university
     */
    public String getUniversity(){
        return university;
    }
    /**
     * setUniversity la phuong thuc cai dat ten truong cho sinh vien
     * @param university co dang string
     * @return void ko tra ve
     */
    public void setUniversity(String university){
        this.university = university;
    }
    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println(getName() + "'s id is " + getId() + " and studies at " + getUniversity());
    }

}