package Ex3;
/**
 * Cat la class mieu ta dac diem con meo
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
 public class Cat {
    private String sound, color;
    /**
     * khoi tao cac dac diem choCat
     * @param sound la tieng keu Cat
     * @param color la mau sac cua Cat
     */
    public Cat(String sound, String color){
        this.sound = sound;
        this.color = color;
    }

    /**
     * getSound la phuong thuc de lay ra tieng keu meo
     * @return sound
     */
    public String getSound(){
        return sound;
    }
    /**
     * setSound la phuong thuc de cai dat tieng keu meo
     * @param sound tieng keu meo
     * @return void ko tra ve
     */
    public void setSound(String sound){
        this.sound = sound;
    }
    /**
     * getColor la phuong thuc de lay ra mau sac meo
     * @return color
     */
    public String getColor(){
        return color;
    }
    /**
     * setColor la phuong thuc de cai dat mau sac meo
     * @param color mau sac meo
     * @return void ko tra ve
     */
    public void setColor(String color){
        this.color = color;
    }
    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println("The Cat says " + getSound() + " and is " + getColor());
    }
}
