package Ex3;

/**
 * Building la class mieu ta dac diem cua toa nha
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Building {
    int floor;
    String name;
    /**
     * khoi tao cac dac diem cho toa nha
     * @param floor la so tang cua toa nha
     * @param name la ten cua toa nha
     * @return ko tra ve gia tri nao
     */
    public Building (int floor,String name){
        this.floor=floor;
        this.name=name;
    }

    /**
     * duoi day la phuong thuc lay ra so tang cua toa nha
     * @return floor
     */
    public int getFloor(){
        return floor;
    }
    /**
     * duoi day la phuong thuc cai dat so tang cua toa nha
     * @param floor co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setFloor(int floor){
        this.floor=floor;
    }
    /**
     * duoi day la phuong thuc lay ra ten cua toa nha
     * @return name
     */
    public String getName(){
        return name;
    }
    /**
     * duoi day la phuong thuc cai dat ten cua toa nha
     * @param name co dang interger
     * @return void ko tra ve gia tri nao
     */
    public void setName(String name){
        this.name=name;
    }

    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println("The " + getName() + " buidling has "+getFloor() + " floors");
    }
}