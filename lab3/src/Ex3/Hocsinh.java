package Ex3;

/**
 * Hocsinh la class mieu ta dac diem cua hoc sinh
 * @author HaMinhHai
 * @version  1.8.1
 * @since 2018/9/22
 */
public class Hocsinh{
    String lop,truong,uniformColor;
    /**
     * khoi tao cac dac diem cho hoc sinh
     * @param lop la ten lop cua hoc sinh
     * @param uniformColor la mau sac dong phuc cua hoc sinh
     * @param truong la ten truong dai hoc cua hoc sinh
     * @return ko tra ve gia tri nao
     */
    public Hocsinh(String lop,String uniformColor,String truong){
        this.lop=lop;
        this.uniformColor =uniformColor;
        this.truong=truong;
    }
    /**
     * duoi day la phuong thuc lay ra ten lop cua hoc sinh
     * @return lop
     */
    public String getLop(){
        return lop;
    }
    /**
     * duoi day la phuong thuc cai dat ten lop cho hoc sinh
     * @param  lop co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setLop(String lop){
        this.lop=lop;
    }
    /**
     * duoi day la phuong thuc lay ra mau sac dong phuc cua
     * hoc sinh
     * @return uniformColor
     */
    public String getUniformColor(){
        return uniformColor;
    }
    /**
     * duoi day la phuong thuc cai dat mau sac dong phuc
     * cho hoc sinh
     * @param  uniformColor co dang string
     * @return void ko tra ve gia tri nao
     */
    public void setUniformColor(String uniformColor){
        this.uniformColor=uniformColor;
    }
    /**
     * duoi day la phuong thuc lay ra ten truong cua hoc sinh
     * @return truong
     */
    public String gettruong(){
        return truong;
    }
    /**
     * duoi day la phuong thuc cai dat ten truong cho hoc sinh
     * @param  truong co dang string
     * @return void ko tra ve gia tri nao
     */
    public void settruong(String truong){
        this.truong=truong;
    }
    /**
     * in ra thuoc tinh
     */
    public void print()
    {
        System.out.println("This sudent studies " + getLop() +"," + gettruong() + " and wears "+ getUniformColor() +" uniform");
    }
}
