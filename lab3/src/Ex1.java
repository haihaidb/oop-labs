/**
 * ex1 la phuong thuc de tim uoc chung lon nhan cua 2 so nguyen
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-09-22
 */

import java.util.Scanner;

public class Ex1 {
    /**
     * UCLN la ham de tim uoc chung lon nhat cua 2 so nguyen
     * @param a la so nguyen thu nhat
     * @param b la so nguyen thu hai
     * @return uoc chung lon nhat cua a va b
     */
    static int UCLN(int a,int b){
        a = Math.abs(a);
        b = Math.abs(b);
        if(a==0 || b==0)
            return a+b;
        while (a!=b) {
            if (a > b)
                a = a - b;
            else
                b = b -a;

        }
        return a;
    }

    /**
     * Tinh fibonaxi cua 1 so nguyen
     * @param n so nguyen
     */
    private static void FIBO(int n)
    {
        int i,s,s1=0,s2=1;
        System.out.print(s1+ " "+s2 + " ");
        for(i=2;i<n;i++){
            s=s1 +s2;
            System.out.print(s+" ");
            s1=s2;
            s2=s;
        }
    }

    /**
     * main la ham thuc hien phuong thuc
     * @param args khong su dung
     */
    public  static  void main(String[] args){
        Scanner sn = new Scanner(System.in);

        //Tim UCLN
        int a ,b;
        a=sn.nextInt();
        b=sn.nextInt();
        System.out.println("UCLN cua a & b la :" + UCLN(a,b));


        //Tinh Fibonaxi
        System.out.println("Day fibonaxi can tim la: ");
        FIBO(3);
    }
}
