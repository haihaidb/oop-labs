/**
 * lop thuoc tinh cho hinh tron
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

import java.util.ArrayList;

public class Circle extends Shape {
    final private double pi = Math.PI;
    private double posx,posy;
    private double radius;

    /**
     * Constructor
     * @param color mau sac
     * @param filled to mau
     * @param pos vi tri
     * @param radius ban kinh
     */
    public Circle(String color, boolean filled, ArrayList<Location> pos,double radius){
        super(color,filled,pos);
        this.radius=radius;

    }

    /**
     * setter
     * @param radius ban kinh
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * getter
     * @return ban kinh
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Info cua hinh tron
     * @return
     */
    public String toString()
    {
        double cv = 2*this.getRadius()*pi;
        double dt = Math.pow(this.getRadius(),2)*pi;
        return
                "\nHinh Tron \n"+
                        "Mau Sac:"+this.getColor()+"\n"+
                        "To mau: "+ this.getFilled() +"\n"+
                        "Ban kinh:" +this.getRadius()+"\n"+
                        "Chu vi: " + cv + "\n"+
                        "Dien tich: "+dt+ "\n";

    }




}
