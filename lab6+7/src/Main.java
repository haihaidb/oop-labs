import java.util.ArrayList;

/**
 * Main la ung dung ve bai tap da hinh kiem tra
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Location> position = new ArrayList<>();

        //CIRCLE
        position.add(new Location(Math.random() * 10, Math.random() * 10));
        Shape c = new Circle("yellow", false, position, 3.5);

        //TRIANGLE
        position.clear();
        for (int i = 0; i < 3; i++) {
            position.add(new Location(Math.random() * 10, Math.random() * 10));
        }
        Shape t = new Triangle("red", true, position);

        //SQUARE
        position.clear();
        for (int i = 0; i < 4; i++) {
            position.add(new Location(Math.random() * 10, Math.random() * 10));
        }
        Shape s = new Square("black", false,position,4.0 );

        //RECTANGLE
        position.clear();
        for (int i = 0; i < 4; i++) {
            position.add(new Location(Math.random() * 10, Math.random() * 10));
        }
        Shape r = new Rectangle("white",false,position,3,4);

        //HEXAGON
        position.clear();
        for (int i = 0; i < 6; i++) {
            position.add(new Location(Math.random() * 10, Math.random() * 10));
        }
        Shape h = new Hexagon("pink",true,position,1.2);

        /*
         * khoi tao cac shape
         */
        ArrayList<Layer> layers = new ArrayList<>();
        ArrayList<Shape> s1 = new ArrayList<>();
        ArrayList<Shape> s2 = new ArrayList<>();
        ArrayList<Shape> s3 = new ArrayList<>();
        /*
         * them cac hinh vao shape
         */
        s1.add(c);
        s1.add(c);
        s1.add(c);
        s1.add(c);

        s2.add(c);
        s2.add(r);
        s2.add(h);
        s2.add(s);
        s2.add(h);

        s3.add(t);
        s3.add(s);
        s3.add(t);
        /*
         * them cac shape vao layer
         */
        layers.add(new Layer(s1,true));
        layers.add(new Layer(s2,true));
        layers.add(new Layer(s3,false));
        /*
         * bat dau thuc thi cac yeu cau
         */
        Diagram diagram = new Diagram(layers);
        System.out.println("Trước khi xóa");
        diagram.xuat();
        layers.get(1).xoaTamgiac();
        layers.get(2).xoaHinhtron();
        layers.get(0).xoaTamgiac();

        System.out.println("Sau khi xóa");
        diagram.xuat();
    }



}
