/**
 * diagram la ung dung xoa tat ca doi tuong trong lop circle trong diagram
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

import java.util.ArrayList;

public class Diagram {
    private ArrayList<Layer> array;

    public Diagram(ArrayList<Layer> array) {
        this.array = array;
    }

    /**
     * in ra man hinh
     */
    public void xuat() {
        for (Layer anArray : array) {
            anArray.xuat();
            System.out.println();
        }
    }

}





