/**
 * Layer la lop xoa tat ca doi tuong trong lop Triangle thuoc lop
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

import java.util.ArrayList;

public class Layer {
    private ArrayList<Shape> array;
    private boolean visible;

    /**
     * Constructor
     * @param array day cac layer
     * @param visible co the ve hay ko
     */
    public Layer(ArrayList<Shape> array,boolean visible) {
        this.array = array;
        this.visible=visible;
    }
    public ArrayList<Shape> getShape() {
        return array;
    }


    /**
      * xoa tam giac ra khoi layer
     */
    public void xoaTamgiac() {
        int i = 0;
        while (i< array.size()) {
            if(array.get(i) instanceof Triangle) {
                array.remove(i);
            }
            i++;
        }
    }

    /**
     * xoa hinh tron ra khoi layer
     */
    public void xoaHinhtron() {
        int i = 0;
        while (i< array.size()) {
            if(array.get(i) instanceof Circle) {
                array.remove(i);
            }
            i++;
        }
    }

    /**
     * in ra man hinh sau khi xoa
     */
    public void xuat() {
        for (Shape anArray : array) {
            System.out.println(anArray);
        }
    }
}


