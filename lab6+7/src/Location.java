/**
 * Tao vi tri cho cac hinh
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

public class Location {
    private double posx, posy;

    /**
     * Constructor
     * @param x xpos
     * @param y ypos
     */
    public Location(double x, double y) {
        this.posx = x;
        this.posy = y;
    }

    /**
     * setter
     * @param posx vi tri x
     */
    public void setPosx(double posx) {
        this.posx = posx;
    }

    /**
     * getter
     * @return vi tri x
     */
    public double getPosx() {
        return posx;
    }

    /**
     * setter
     * @param posy vi tri y
     */
    public void setPosy(double posy) {
        this.posy = posy;
    }
    /**
     * getter
     * @return vi tri y
     */
    public double getPosy() {
        return posy;
    }

}
