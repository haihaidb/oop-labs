/**
 * khoi tao hinh luc giac
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

import java.util.ArrayList;

public class Hexagon extends Shape {
    private double side;

    /**
     * Constructor
     * @param color mau sac
     * @param filled to mau
     * @param pos vi tri
     * @param side canh
     */
    public Hexagon(String color, boolean filled, ArrayList<Location> pos, double side) {
        super(color, filled, pos);
        this.side = side;
    }

    /**
     * getter
     * @return canh
     */
    public double getSide() {
        return side;
    }

    /**
     * setter
     * @param side canh
     */
    public void setSide(double side) {
        this.side = side;
    }

    /**
     * Info cua hexagon
     * @return
     */
    public String toString() {
        return
                "\nLuc Giac Deu\n" +
                "Mau sac: " + this.getColor() + "\n"+
                "To Vien: " + this.getFilled() + "\n"+
                "Canh: " + this.getSide() + "\n";
    }

}
