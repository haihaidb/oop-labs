/**
 * Khoi tao thuoc tinh cua 1 hinh
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

import java.util.ArrayList;

public class Shape
{

    private String color = "red";
    private boolean filled = true;
    private ArrayList<Location> p;

    /**
     * Constructor
     * @param color mau sac
     * @param filled to mau
     * @param pos vi tri
     */
    public Shape(String color,boolean filled,ArrayList<Location> pos)
    {
        this.color=color;
        this.filled=filled;
        this.p=pos;

    }

    /**
     * getter
     * @return vi tri
     */
    public ArrayList<Location> getPoints() {
        return this.p;}

    /**
     * setColor la ham setter de cai dat gia tri cho color
     * @param color bien dang String
     * @return void ko tra ve
     */
    void setColor(String color) {
        this.color = color;
    }
    /**
     * setFilled la ham setter de cai dat gia tri cho filled
     * @param filled bien dang boolean
     * @return void ko tra ve
     */
    void setFilled(boolean filled) {
        this.filled = filled;
    }
    /**
     * getColor la ham getter de goi ra gia tri cua color
     * @return  color
     */
    String getColor() {
        return color;
    }
    /**
     * getFilled la ham getter de goi ra gia tri cua filled
     * @return filled
     */
    boolean getFilled() {
        return filled;
    }


}
