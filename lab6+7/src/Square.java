/**
 * Khoi tao hinh vuong
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

import java.util.ArrayList;

public class Square extends Shape {

    private double side;

    /**
     * Constructor
     * @param color mau sac
     * @param filled to mau
     * @param pos vi tri
     * @param side do dai canh
     */
    public Square(String color, boolean filled, ArrayList<Location> pos, double side) {
        super(color, filled, pos);
        this.side = side;
    }

    /**
     * getter
     * @return canh
     */
    public double getSide() {
        return side;
    }

    /**
     * setter
     * @param side canh
     */
    public void setSide(double side) {
        this.side = side;
    }

    /**
     * Infor cua hv
     * @return
     */
    public String toString() {
        return
                "\nHinh Vuong\n" +
                "Mau sac: " + this.getColor() + "\n"+
                "To Vien: " + this.getFilled() + "\n"+
                "Canh: " + this.getSide() + "\n";

    }

}
