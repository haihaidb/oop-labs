/**
 * Khoi tao hinh tam giac
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018-10-09
 */

import java.util.ArrayList;

public class Triangle extends Shape {

    /**
     * Constructor
     * @param color mau sac
     * @param filled to mau
     * @param pos vi tri
     */
    public Triangle(String color, boolean filled, ArrayList<Location> pos) {
        super(color, filled, pos);
    }
        public String toString ()

    {

        return
                "\nHinh Tam Giac \n"+
                        "Mau Sac:"+this.getColor()+"\n"+
                        "To mau: "+ this.getFilled() +"\n";
    }



}
