/**
 * lab04 la 1 ung dung de tim ra GTLN cua 2 so nguyen,
 * phan tu co GTNN trong 1 mang va tinh chi so BMI
 * @author HaMinhHai
 * @version 10.0.1
 * @since 2018-09-25
 */
package com.hmhai.maven.junittest;

class lab04 {
    /**
     * GTLN la ham static de tim ra GTLN cua 2 so nguyen
     * @param a first integer
     * @param b second integer
     * @return GTLN
     */
    static int GTLN(int a, int b) {
        if(a>=b) {
            System.out.println(a);
            return  a;
        } else {
            System.out.println(b);
            return  b;
        }
    }

    /**
     * GTNN la ham static de tim phan tu nho nhat trong mang
     * @param ar la 1 mang
     * @return GTNN
     */
    static  int GTNN(int[] ar) {
        int res = ar[0];
        for (int i=1; i<ar.length; i++)
            if (ar[i]<res) {
                res = ar[i];
            }
        System.out.println(res);
        return res;
    }

    /**
     * BMI la phuong thuc tinh chi so BMI
     * @param height la chieu cao(m)
     * @param weight la can nang(kg)
     * @return tinh trang suc khoe
     */
    static String BMI(double height, double weight) {
        double bmi = weight / (Math.pow(height,2));
        String kq;

        if (bmi < 18.5) {
            kq ="Thieu can";
        }else if ((bmi >= 18.5) && (bmi <= 22.99)) {
            kq = "Binh thuong";
        }else if ((bmi >= 23.00) && (bmi < 24.99)) {
            kq = "Thua can";
        }else
            kq = "Beo phi";

        return kq;

    }
}

