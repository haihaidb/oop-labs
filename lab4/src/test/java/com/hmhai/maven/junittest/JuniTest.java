/**
 * ung dung de test xem chuong trinh chay dung ko
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018/9/25
 */
package com.hmhai.maven.junittest;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class JuniTest {

    @Test
    public void testGTLN1() {
        int s1 = lab04.GTLN(2, 3);
        assertEquals(3, s1);
    }
    @Test
    public void testGTLN2() {
        int s2 = lab04.GTLN(20, 36);
        assertEquals(36, s2);
    }
    @Test
    public void testGTLN3() {
        int s3 = lab04.GTLN(2, 2);
        assertEquals(2, s3);
    }
    @Test
    public void testGTLN4() {
        int s4 = lab04.GTLN(100, 1);
        assertEquals(100, s4);
    }
    @Test
    public void testGTLN5() {
        int s5 = lab04.GTLN(98, 99);
        assertEquals(99, s5);
    }

    @Test
    public void testGTNN1() {
        int[] ar1 = new int[]{2, -10, -25, 302, 306, 14, 0, 15, 99, 85, 67, 16};
        int s1 = lab04.GTNN(ar1);
        assertEquals(-25, s1);
    }
    @Test
    public void testGTNN2() {
        int[] ar2 = new int[]{-19, 103, 94, 104, -94, 59, 4, 30, 6, -99, 15, 22};
        int s2 = lab04.GTNN(ar2);
        assertEquals(-99, s2);
    }
    @Test
    public void testGTNN3() {
        int[] ar3 = new int[]{211, -103, 18, 133, 316, -29, 41, 61, -53, 33, 567, -123};
        int s3 = lab04.GTNN(ar3);
        assertEquals(-123, s3);
    }
    @Test
    public void testGTNN4() {
        int[] ar4 = new int[]{324, 52, 4, 54, 435, 178, 23, 234, 60, 56, 5, 654};
        int s4 = lab04.GTNN(ar4);
        assertEquals(4, s4);
    }
    @Test
    public void testGTNN5() {
        int[] ar5 = new int[]{234, -105, -254, 30, 6, 142, 10, 157, 23, 34, 634, 116};
        int s5 = lab04.GTNN(ar5);
        assertEquals(-254, s5);
    }
    @Test
    public void testBMI1() {
        String s1 = lab04.BMI(1.65,63) ;
        assertEquals("Thua can", s1);
    }
    @Test
    public void testBMI2() {
        String s2 = lab04.BMI(1.78,68);
        assertEquals("Binh thuong", s2);
    }
    @Test
    public void testBMI3() {
        String s3 = lab04.BMI(1.60,41);
        assertEquals("Thieu can",s3);
    }
    @Test
    public void testBMI4() {
        String s4 = lab04.BMI(1.50,50);
        assertEquals("Binh thuong", s4);
    }
    @Test
    public void testBMI5() {
        String s5 = lab04.BMI(1.70,48);
        assertEquals("Thieu can", s5);
    }

}


