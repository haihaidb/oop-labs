/**
 * la method sort cac kieu du lieu nguyen thuy va find max cua 1 day~
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018/11/27
 */

import java.util.ArrayList;

public class PrimitiveType {

    /**
     * sap xep 1 mang
     * @param array mang truyen vao
     * @param <T> kieu du lieu
     * @return day da duoc sap xep
     */

    public <T extends Comparable> T[] sortedArray(T[] array)
    {
        int i = 0;
        while (i < array.length) {
            for (int j = i+1; j < array.length; j++)
            {
                if(array[i].compareTo(array[j]) > 0)
                {
                    T template = array[i];
                    array[i] = array[j];
                    array[j] = template;
                }
            }
            i++;
        }

            return array;
    }

    /**
     * ham tim so lon nhat cua
     * @param listArray mang truyen vao
     * @param <T> kieu du lieu
     * @return
     */

    public <T extends Comparable> T MaxNumber(ArrayList<T> listArray)
    {
        if (listArray.isEmpty())
            return null;

        T nmax = listArray.get(0);

        int t = 0, listArraySize = listArray.size();
        while (t < listArraySize) {
            T i = listArray.get(t);
            if (i.compareTo(nmax) > 0)
                nmax = i;
            t++;
        }
        return nmax;
    }

}
