/**
 * chuong trinh chay thu
 * @author HaMinhHai
 * @version 1.8.1
 * @since 2018/11/27
 */

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        PrimitiveType prim = new PrimitiveType();

        Float[] ar = {1.5f, 0.4f, -1.0f, -5.1f, -10.9f, 100.0f};

        System.out.print("Before sorted:");
        for(int i = 0; i < ar.length; i++)
            System.out.print(ar[i] + " ");

        ar = prim.sortedArray(ar);

        ArrayList<Float> array = new ArrayList<>();
        array.add(1.5f);
        array.add(0.4f);
        array.add(-1.0f);
        array.add(-5.1f);
        array.add(-10.9f);
        array.add(100.0f);

        System.out.print("\nAfter sorted:");

        for(int i = 0; i < ar.length; i++)
            System.out.print(ar[i] + " ");

        System.out.print("\nMaxNumber = " + prim.MaxNumber(array));







    }
}
