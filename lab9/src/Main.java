/**
 * @author HAMINHHAI
 * @version 1.8.2
 * @since 2018-10-30
 */
import java.util.Scanner;


public class Main {
    /**
     * la ham xu ly cac yeu cau trong class IOFile
     * @param args no use
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        final String path = "C:\\Users\\MinhHai3069\\IdeaProjects\\week9\\src\\in.txt";
        final String folderPath = "C:\\Users\\";
        /*
         * in ra noi dung file
         */
        String read = IOFile.readContentFromFile(path);
        System.out.println(read);
        /*
         * ghi de/ghi vao cuoi file
         */
        IOFile.writeContentToFile(path);
        /*
         * seatch file by name
         */
        System.out.print("Nhap ten file muon tim kiem:");
        String filekey = sc.nextLine();
        System.out.println("Searching .... ");
        IOFile.findFileByName(folderPath, filekey);

    }
}