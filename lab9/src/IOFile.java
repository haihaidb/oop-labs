/**
 * la 1 chuong trinh thao tac voi file
 * @author HAMINHHAI
 * @version 1.8.2
 * @since 2018-10-30
 */

import java.io.*;
import java.util.Scanner;

public class IOFile {
    /**
     * ham nay dung de doc file theo duong dan da cho
     * @param path duong dan toi file
     * @return noi dung trong file
     */
    public static String readContentFromFile(String path){
        BufferedReader br = null;
        String curline = null;

        try {
            br = new BufferedReader(new FileReader(path));
            int i=1;
            while ((curline = br.readLine()) != null) {

                System.out.println("Noi dung trong file dong "+i+": "+curline);
                i++;
            }
        } catch (IOException e) {
            System.err.println("An IOException was caught :" + e.getMessage());
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
    /**
     * ham nay dung de ghi them noi dung vao cuoi file
     * @param path duong dan toi file
     */
    public static void writeContentToFile(String path){
        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            System.out.print("Nhap noi dung can ghi vao cuoi file: ");
            //System.out.print("Nhap noi dung muon ghi de: ");
            Scanner sc = new Scanner(System.in);
            String data = sc.nextLine();
            File file = new File(path);
            //kiem tra neu file chua co thi tao file moi
            if (!file.exists()) {
                file.createNewFile();
            }
            /*
             * append: true se xoa toan bo noi dung va ghi noi dung moi
             * append: false se ghi de vao cuoi file ma ko xoa noi dung
             */
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            //bw.write("\n");
            bw.write(data);
            System.out.println("Xong!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * ham nay dung de tim kiem 1 tep trong thu muc
     * @param folderPath duong dan toi folder
     * @param fileName ten file muon tim kiem
     */

    public static void findFileByName(String folderPath, String fileName) {
        File folder = new File(folderPath);
        /*
        * kiem tra file co ton tai hay ko
        * neu ton tai thi kiem tra neu la file lay ten file so sanh voi
        fileName truyen vao
        * neu giong thi in ra duong dan
         */

        if (folder.exists()) {
            if (folder.isDirectory()) {
                if (folder.getName().endsWith(fileName)) {
                    System.out.println(folder.getAbsolutePath());
                }
                /*
                 * neu duong dan la 1 folder thi duyet listfile sau do de quy findFindByName
                 */
                File[] listFile;
                listFile = folder.listFiles();

                if (listFile != null) {
                    for (File f : listFile) {
                        findFileByName(f.getAbsolutePath(), fileName);
                    }
                } else
                    System.out.println("Khong tim thay file trung khop!");
            } else {
                System.out.println("Khong phai folder!");
                }

        } else
                System.out.println("File ko ton tai!");

    }
}





